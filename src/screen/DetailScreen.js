import React from 'react';
import {Image, Text, TouchableOpacity, View, ScrollView,} from 'react-native';
import {styles} from './Style';

const DetailScreen = ({navigation, route}) => {
  return (
    <View style={styles.container}>
      <ScrollView //component yang digunakan agar tampilan kita bisa discroll
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{paddingBottom: 10}}>
        <View style={{width: '100%'}}>
        <View style={{justifyContent: 'space-between',
        flexDirection: 'row',
        alignItems: 'center',
        paddingRight: 7,}}>
              <TouchableOpacity onPress={() => navigation.navigate('Home')}>
                <Image
                  source={require('../assets/icon/back_ic.png')} //load asset dari local
                  style={{
                    width: 16,
                    height: 12,
                    resizeMode: 'contain',
                  }}
                />
              </TouchableOpacity>
              <TouchableOpacity>
                <Image
                  source={require('../assets/icon/whiteBag_ic.png')} //load asset dari local
                  style={{
                    width: 24,
                    height: 24,
                    resizeMode: 'contain',
                  }}
                />
              </TouchableOpacity>
            </View>
          <Image
            source={require('../assets/image/image_seturan.png')} //load asset dari local
            style={{
              width: '100%',
              height: 316,
            }}
          />
        </View>
        <View
          style={{
            paddingHorizontal: 20,
            paddingBottom: 34,
            paddingTop: 24,
            marginTop: -30,
            borderTopRightRadius: 20,
            borderTopLeftRadius: 20,
            backgroundColor: '#FFFFFF',
          }}>
          <Text style={{fontSize: 18, fontWeight: '700', color: '#201F26'}}>
            Jack Repair Seturan
          </Text>
          <View style={styles.stars}>
            <Image
              source={require('../assets/icon/fullstar_ic.png')} //load asset dari local
              style={{
                width: 15.75,
                height: 15,
                resizeMode: 'contain',
              }}
            />
            <Image
              source={require('../assets/icon/fullstar_ic.png')} //load asset dari local
              style={{
                width: 15.75,
                height: 15,
                resizeMode: 'contain',
              }}
            />
            <Image
              source={require('../assets/icon/fullstar_ic.png')} //load asset dari local
              style={{
                width: 15.75,
                height: 15,
                resizeMode: 'contain',
              }}
            />
            <Image
              source={require('../assets/icon/fullstar_ic.png')} //load asset dari local
              style={{
                width: 15.75,
                height: 15,
                resizeMode: 'contain',
              }}
            />
            <Image
              source={require('../assets/icon/linestar_ic.png')} //load asset dari local
              style={{
                width: 15.75,
                height: 15,
                resizeMode: 'contain',
              }}
            />
          </View>
          <View style={styles.location}>
            <Image
              source={require('../assets/icon/Location_ic.png')} //load asset dari local
              style={{
                width: 14,
                height: 18,
                resizeMode: 'contain',
                marginVertical: 5,
                marginRight: 20,
              }}
            />
            <View>
              <Text
                style={{
                  fontSize: 10.5,
                  fontWeight: '400',
                  color: '#979797',
                }}>
                Jalan Affandi (Gejayan), No.15, Sleman{' '}
              </Text>
              <Text
                style={{
                  fontSize: 10.5,
                  fontWeight: '400',
                  color: '#979797',
                }}>
                Yogyakarta, 55384
              </Text>
            </View>
            <TouchableOpacity>
              <Text
                style={{
                  fontSize: 12.5,
                  fontWeight: '700',
                  color: '#3471CD',
                  marginLeft: 90,
                }}>
                Lihat Maps
              </Text>
            </TouchableOpacity>
          </View>
          <View style={styles.times}>
            <TouchableOpacity>
              <View
                style={{
                  width: 55,
                  height: 21,
                  borderRadius: 10.5,
                  backgroundColor: 'rgba(17, 168, 78, 0.12)',
                  alignItems: 'center',
                  justifyContent: 'center',
                  marginVertical: 5,
                }}>
                <Text
                  style={{
                    fontSize: 12,
                    color: '#11A84E',
                    fontWeight: '700',
                  }}>
                  BUKA
                </Text>
              </View>
            </TouchableOpacity>
            <Text
              style={{
                fontSize: 12,
                color: '#343434',
                fontWeight: '700',
                marginLeft: 15,
              }}>
              09:00 - 21:00
            </Text>
          </View>
          <View
            style={{
              marginVertical: 10,
              backgroundColor: '#EEEEEE',
              width: '100%',
              height: 1,
            }}
          />
          <View style={styles.description}>
            <Text
              style={{
                fontSize: 16,
                color: '#201F26',
                fontWeight: '500',
                marginTop: 5,
              }}>
              Deskripsi
            </Text>
            <Text
              style={{
                fontSize: 14,
                color: '#595959',
                fontWeight: '400',
                marginVertical: 10,
              }}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Massa
              gravida mattis arcu interdum lectus egestas scelerisque. Blandit
              porttitor diam viverra amet nulla sodales aliquet est. Donec enim
              turpis rhoncus quis integer. Ullamcorper morbi donec tristique
              condimentum ornare imperdiet facilisi pretium molestie.
            </Text>
            <Text
              style={{
                fontSize: 16,
                color: '#201F26',
                fontWeight: '500',
              }}>
              Range Biaya
            </Text>
            <Text
              style={{
                fontSize: 16,
                color: '#8D8D8D',
                fontWeight: '500',
                marginVertical: 5,
              }}>
              Rp 20.000 - 80.000
            </Text>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

export default DetailScreen;
