import React from 'react';
import {
  View,
  Text,
  TextInput,
  ScrollView,
  Image,
  TouchableOpacity,
} from 'react-native';
import {styles} from './Style';

const Checkout = ({navigation, route}) => {
  return (
    <View style={styles.container}>
      <View
        style={{
          width: '100%',
          height: 60,
          backgroundColor: '#FFFFFF',
          flexDirection: 'row',
          alignSelf: 'center',
          alignItems: 'center',
          paddingLeft: 14,
          shadowColor: '#000',
          shadowOffset: {
            width: 0,
            height: 1,
          },
          shadowOpacity: 0.22,
          shadowRadius: 2.22,

          elevation: 3,
        }}>
        <TouchableOpacity onPress={() => navigation.navigate('Summary')}> 
          <Image
          style={{
            size: 24,
            color: '#BB2427',
          }}
          source={require('../assets/icon/blackBack_ic.png')}
          />
        </TouchableOpacity>
        <Text
          style={{
            fontFamily: 'Montserrat',
            marginLeft: 14,
            fontWeight: '700',
            fontSize: 18,
            lineHeight: 17,
          }}>
          Checkout
        </Text>
      </View>
      <ScrollView //component yang digunakan agar tampilan kita bisa discroll
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{paddingBottom: 10}}>
        <View
          style={{
            width: '100%',
            height: 172,
            backgroundColor: '#FFFFFF',
            alignSelf: 'center',
            flexDirection: 'column',
            marginVertical: 10,
          }}>
          <Text
            style={{
              paddingTop: 10,
              marginLeft: 14,
              fontFamily: 'Monsserrat',
              fontWeight: '500',
              fontSize: 14,
              lineHeight: 17,
              color: '#979797',
            }}>
            Data Customer
          </Text>
          <Text
            style={{
              fontFamily: 'Monsserrat',
              fontWeight: '400',
              fontSize: 14,
              lineHeight: 17,
              color: '#000000',
              marginLeft: 14,
              paddingTop: 10,
            }}>
            Fahrii Fahriza (081357088476)
          </Text>
          <Text
            style={{
              fontFamily: 'Monsserrat',
              fontWeight: '400',
              fontSize: 14,
              lineHeight: 17,
              color: '#000000',
              marginLeft: 14,
              paddingTop: 6,
            }}>
            Taman Lucida No. 5, Warungboto, Umbulharjo, Yogyakarta
          </Text>
          <Text
            style={{
              fontFamily: 'Monsserrat',
              fontWeight: '400',
              fontSize: 14,
              lineHeight: 17,
              color: '#000000',
              marginLeft: 14,
              paddingTop: 6,
            }}>
            fahriifahriza@gmail.com
          </Text>
        </View>
        <View
          style={{
            width: '100%',
            height: 107,
            backgroundColor: '#FFFFFF',
            alignSelf: 'center',
            flexDirection: 'column',
            backgroundColor: '#FFFFFF',
            marginVertical: 10,
          }}>
          <Text
            style={{
              paddingTop: 10,
              marginLeft: 14,
              fontFamily: 'Monsserrat',
              fontWeight: '500',
              fontSize: 14,
              lineHeight: 17,
              color: '#979797',
            }}>
            Alamat Outlet Tujuan
          </Text>
          <Text
            style={{
              fontFamily: 'Monsserrat',
              fontWeight: '400',
              fontSize: 14,
              lineHeight: 17,
              color: '#000000',
              marginLeft: 14,
              paddingTop: 10,
            }}>
            Jack-Repair - Seturan (0983243248)
          </Text>
          <Text
            style={{
              fontFamily: 'Monsserrat',
              fontWeight: '400',
              fontSize: 14,
              lineHeight: 17,
              color: '#000000',
              marginLeft: 14,
              paddingTop: 6,
            }}>
            Jalan Corongan Maguwoharjo Depok
          </Text>
        </View>

        <View
          style={{
            width: '100%',
            height: 172,
            backgroundColor: '#FFFFFF',
            alignSelf: 'center',
            flexDirection: 'column',
            backgroundColor: '#FFFFFF',
            marginVertical: 10,
          }}>
          <Text
            style={{
              paddingTop: 10,
              marginLeft: 14,
              fontFamily: 'Monsserrat',
              fontWeight: '500',
              fontSize: 14,
              lineHeight: 17,
              color: '#979797',
            }}>
            Barang
          </Text>
          <View
            style={{
              width: '87%',
              height: 133,
              backgroundColor: '#FFFFFF',
              alignSelf: 'flex-start',
              marginLeft: 14,
              flexDirection: 'row',
              marginVertical: 5,
            }}>
            <Image
              style={{alignSelf: 'center',}}
              source={require('../assets/image/sample_sepatu.png')}></Image>
            <View
              style={{
                flexDirection: 'column',
                alignSelf: 'center',
                paddingLeft: 13,
              }}>
              <Text
                style={{
                  fontFamily: 'Monsserrat',
                  fontWeight: '500',
                  fontSize: 12,
                  lineHeight: 15,
                  color: '#000000',
                }}>
                New Balance - Pink Abu - 40
              </Text>
              <Text
                style={{
                  fontFamily: 'Monsserrat',
                  fontWeight: '400',
                  fontSize: 12,
                  lineHeight: 15,
                  color: '#737373',
                  paddingTop: 11,
                }}>
                Cuci sepatu
              </Text>
              <Text style={{paddingTop: 10}}>Note : -</Text>
            </View>
          </View>
        </View>

        <View
          style={{
            width: '100%',
            height: 107,
            backgroundColor: '#FFFFFF',
            alignSelf: 'center',
            flexDirection: 'column',
            backgroundColor: '#FFFFFF',
            height: 130,
            marginVertical: 10,
          }}>
          <Text
            style={{
              paddingTop: 10,
              marginLeft: 14,
              fontFamily: 'Monsserrat',
              fontWeight: '500',
              fontSize: 14,
              lineHeight: 17,
              color: '#979797',
            }}>
            Rincian Pembayaran
          </Text>
          <View style={{flexDirection: 'row'}}>
            <Text
              style={{
                fontFamily: 'Monsserrat',
                fontWeight: '400',
                fontSize: 12,
                lineHeight: 15,
                color: '#000000',
                marginLeft: 14,
                paddingTop: 10,
              }}>
              Cuci Sepatu
            </Text>
            <Text
              style={{
                fontFamily: 'Monsserrat',
                fontWeight: '500',
                fontSize: 12,
                lineHeight: 15,
                color: '#FFC107',
                marginLeft: 14,
                paddingTop: 10,
              }}>
              x1 Pasang
            </Text>
            <Text
              style={{
                fontFamily: 'Monsserrat',
                fontWeight: '400',
                fontSize: 12,
                lineHeight: 15,
                color: '#000000',
                marginLeft: 174,
                paddingTop: 10,
              }}>
              Rp 30.000
            </Text>
          </View>

          <View style={{flexDirection: 'row'}}>
            <Text
              style={{
                fontFamily: 'Monsserrat',
                fontWeight: '400',
                fontSize: 12,
                lineHeight: 15,
                color: '#000000',
                marginLeft: 14,
                marginRight: 80,
                paddingTop: 10,
              }}>
              Biaya Antar
            </Text>
            <Text
              style={{
                fontFamily: 'Monsserrat',
                fontWeight: '400',
                fontSize: 12,
                lineHeight: 15,
                color: '#000000',
                marginLeft: 174,
                paddingTop: 10,
              }}>
              Rp 3.000
            </Text>
          </View>
          <View
            style={{
              borderWidth: 1,
              borderColor: '#EDEDED',
              marginTop: 10,
              marginBottom: 8,
              marginLeft: -16,
              width: 360,
              alignSelf: 'center',
            }}></View>
          <View style={{flexDirection: 'row'}}>
            <Text
              style={{
                fontFamily: 'Monsserrat',
                fontWeight: '400',
                fontSize: 12,
                lineHeight: 15,
                color: '#000000',
                marginLeft: 14,
                marginRight: 106,
                paddingTop: 10,
              }}>
              Total
            </Text>
            <Text
              style={{
                fontFamily: 'Monsserrat',
                fontWeight: '700',
                fontSize: 12,
                lineHeight: 15,
                color: '#034262',
                marginLeft: 174,
                paddingTop: 10,
              }}>
              Rp 33.000
            </Text>
          </View>
        </View>

        <View
          style={{
            width: '100%',
            height: 150,
            backgroundColor: '#FFFFFF',
            alignSelf: 'center',
            flexDirection: 'column',
            backgroundColor: '#FFFFFF',
            marginVertical: 10,
            paddingHorizontal:8,
            paddingTop:8,
            paddingBottom:11,

          }}>
          <Text
            style={{
              paddingTop: 10,
              marginLeft: 14,
              fontFamily: 'Monsserrat',
              fontWeight: '500',
              fontSize: 14,
              lineHeight: 17,
              color: '#979797',
            }}>
            Pilih Pembayaran
          </Text>
          <ScrollView horizontal={true}>
          <View style={{flexDirection: 'row'}}>
              <View style={styles.paymentMethod}>
              <Image 
              source={require('../assets/icon/cash_ic.png')} //load asset dari local
              style={{
                width: 36,
                height: 36,
                resizeMode: 'contain',
                marginTop: 15,
                marginBottom: 7,
              }}
            />
            <Text
              style={{
                fontFamily: 'Monsserrat',
                fontWeight: '400',
                fontSize: 12,
                lineHeight: 15,
                color: '#000000',
                marginBottom: 12,
              }}>
              Bank Transfer
            </Text>
              </View>
              <View style={styles.paymentMethod}>
              <Image 
              source={require('../assets/icon/ovo_ic.png')} //load asset dari local
              style={{
                width: 71,
                height: 22,
                resizeMode: 'contain',
                marginTop: 19,
                marginBottom: 13,
              }}
            />
            <Text
              style={{
                fontFamily: 'Monsserrat',
                fontWeight: '400',
                fontSize: 12,
                lineHeight: 15,
                color: '#000000',
                marginBottom: 12,
              }}>
              OVO
            </Text>
              </View>
              <View style={styles.paymentMethod}>
              <Image 
              source={require('../assets/icon/credit_ic.png')} //load asset dari local
              style={{
                width: 32,
                height: 21,
                resizeMode: 'contain',
                marginTop: 20,
                marginBottom: 13,
              }}
            />
            <Text
              style={{
                fontFamily: 'Monsserrat',
                fontWeight: '400',
                fontSize: 12,
                lineHeight: 15,
                color: '#000000',
                marginBottom: 12,
              }}>
              Bank Transfer
            </Text>
              </View>
            </View>
            </ScrollView>
        </View>

        <View style={styles.cartButton}>
          <TouchableOpacity onPress={() => navigation.navigate('Reservasi')}>
            <Text
              style={{
                fontSize: 16.5,
                color: '#FFFFFF',
                fontWeight: '700',
              }}>
              Pesan Sekarang
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};
export default Checkout;
