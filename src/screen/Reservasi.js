import React from 'react';
import {
  View,
  Text,
  TextInput,
  ScrollView,
  Image,
  TouchableOpacity,
} from 'react-native';
import {styles} from './Style';

const Reservasi = ({navigation, route}) => {
  return (
    <View style={{flex: 1, backgroundColor: '#FFFFFF'}}>
      <View
        style={{
          width: '100%',
          height: 60,
          backgroundColor: '#FFFFFF',
          flexDirection: 'row',
          alignSelf: 'center',
          alignItems: 'center',
          paddingLeft: 14,
          shadowColor: '#000',
          shadowOffset: {
            width: 0,
            height: 1,
          },
          shadowOpacity: 0.22,
          shadowRadius: 2.22,

          elevation: 3,
        }}>
        <TouchableOpacity onPress={() => navigation.navigate('Checkout')}>
          <Image
            style={{
              size: 14,
              color: '#2E3A59',
            }}
            source={require('../assets/icon/Close_ic.png')}
          />
        </TouchableOpacity>
      </View>
      <ScrollView //component yang digunakan agar tampilan kita bisa discroll
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{paddingBottom: 10}}>
        <View style={{justifyContent: 'center', alignItems: 'center'}}>
          <Text
            style={{
              color: '#11A84E',
              paddingTop:135,
              fontFamily: 'Montserrat',
              fontStyle: 'normal',
              fontWeight: '700',
              fontSize: 18,
              lineHeight: 17,
            }}>
            Reservasi Berhasil
          </Text>
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              height: 133.33,
              width: 133.33,
              backgroundColor: '#11A84E',
              mixBlendMode: 'normal',
              opacity: 0.4,
              borderRadius: 30,
              marginTop:53.33,
              marginBottom:68.33,
            }}>
            <Image
              style={{
                color: '#11A84E',
              }}
              source={require('../assets/icon/checklist_ic.png')}
            />
          </View>
          <Text
            style={{
              color: '#000000',
              fontFamily: 'Montserrat',
              fontStyle: 'normal',
              fontWeight: '400',
              fontSize: 18,
              lineHeight: 17,
            }}>
            Kami Telah Mengirimkan Kode
          </Text>
          <Text
            style={{
              color: '#000000',
              fontFamily: 'Montserrat',
              fontStyle: 'normal',
              fontWeight: '400',
              fontSize: 18,
              lineHeight: 17,
              paddingBottom:115,
            }}>
            Reservasi ke Menu Transaksi
          </Text>
        </View>

        <View style={styles.cartButton}>
          <TouchableOpacity onPress={() => navigation.navigate('Transaksi')}>
            <Text
              style={{
                fontSize: 16.5,
                color: '#FFFFFF',
                fontWeight: '700',
              }}>
              Lihat Kode Reservasi
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};
export default Reservasi;
