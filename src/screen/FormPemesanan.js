import React from 'react';
import {
  View,
  Text,
  TextInput,
  ScrollView,
  Image,
  TouchableOpacity,
} from 'react-native';
import {styles} from './Style';
const FormPemesanan = ({navigation, route}) => {
  return (
    <View style={{flex: 1, backgroundColor: '#FFFFFF'}}>
      <View
        style={{
          width: '100%',
          height: 60,
          backgroundColor: '#FFFFFF',
          flexDirection: 'row',
          alignSelf: 'center',
          alignItems: 'center',
          paddingLeft: 14,
          shadowColor: '#000',
          shadowOffset: {
            width: 0,
            height: 1,
          },
          shadowOpacity: 0.22,
          shadowRadius: 2.22,

          elevation: 3,
        }}>
        <TouchableOpacity onPress={() => navigation.navigate('Home')}>
          <Image
          style={{
            size: 24,
            color: '#BB2427',
          }}
          source={require('../assets/icon/blackBack_ic.png')}
          />
        </TouchableOpacity>
        <Text
          style={{
            fontFamily: 'Montserrat',
            marginLeft: 14,
            fontWeight: '700',
            fontSize: 18,
            lineHeight: 17,
          }}>
          Formulir Pemesanan
        </Text>
      </View>
      <ScrollView //component yang digunakan agar tampilan kita bisa discroll
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{paddingBottom: 10}}>
        <View style={styles.orderForm}>
          <View style={{marginBottom: 25}}>
            <Text style={{color: 'red', fontWeight: 'bold'}}>Merek</Text>
            <TextInput //component yang digunakan untuk memasukkan data yang kita inginkan
              placeholder="Masukan Merk Barang" //pada tampilan ini, kita ingin user memasukkan email
              style={{
                marginTop: 10,
                width: '100%',
                borderRadius: 8,
                backgroundColor: '#F6F8FF',
                paddingHorizontal: 10,
              }}
              keyboardType="default" //akan muncul tombol @ pada keyboard yang nanti akan memudahkan user mengisi email
            />
          </View>
          <View style={{marginBottom: 25}}>
            <Text style={{color: 'red', fontWeight: 'bold'}}>Warna</Text>
            <TextInput //component yang digunakan untuk memasukkan data yang kita inginkan
              placeholder="Warna Barang, cth : Merah - Putih " //pada tampilan ini, kita ingin user memasukkan email
              style={{
                marginTop: 10,
                width: '100%',
                borderRadius: 8,
                backgroundColor: '#F6F8FF',
                paddingHorizontal: 10,
              }}
              keyboardType="default" //akan muncul tombol @ pada keyboard yang nanti akan memudahkan user mengisi email
            />
          </View>
          <View style={{marginBottom: 25}}>
            <Text style={{color: 'red', fontWeight: 'bold'}}>Ukuran</Text>
            <TextInput //component yang digunakan untuk memasukkan data yang kita inginkan
              placeholder="Cth : S, M, L / 39,40" //pada tampilan ini, kita ingin user memasukkan email
              style={{
                marginTop: 10,
                width: '100%',
                borderRadius: 8,
                backgroundColor: '#F6F8FF',
                paddingHorizontal: 10,
              }}
              keyboardType="default" //akan muncul tombol @ pada keyboard yang nanti akan memudahkan user mengisi email
            />
          </View>
          <View style={{marginBottom: 25}}>
            <Text style={{color: 'red', fontWeight: 'bold'}}>Photo</Text>
            <View style={styles.photoBox}>
              <Image
                source={require('../assets/icon/Camera_ic.png')} //load asset dari local
                style={{
                  width: 20,
                  height: 18,
                  marginBottom: 10,
                }}
              />
              <Text
                style={{
                  fontSize: 12,
                  color: '#BB2427',
                  fontWeight: '400',
                  marginTop: -1,
                }}>
                Add Photo
              </Text>
            </View>
            <View style={styles.checkBox}>
              <View
                style={{
                  borderWidth: 1,
                  borderRadius: 3,
                  borderColor: '#B8B8B8',
                  height: 24,
                  width: 24,
                }}></View>
              <Text
                style={{
                  fontSize: 14,
                  color: '#201F26',
                  fontWeight: '400',
                  marginLeft: 23,
                }}>
                Ganti Sol Sepatu
              </Text>
            </View>
            <View style={styles.checkBox}>
              <View
                style={{
                  borderWidth: 1,
                  borderRadius: 3,
                  borderColor: '#B8B8B8',
                  height: 24,
                  width: 24,
                }}></View>
              <Text
                style={{
                  fontSize: 14,
                  color: '#201F26',
                  fontWeight: '400',
                  marginLeft: 23,
                }}>
                Jahit Sepatu
              </Text>
            </View>
            <View style={styles.checkBox}>
              <View
                style={{
                  borderWidth: 1,
                  borderRadius: 3,
                  borderColor: '#B8B8B8',
                  height: 24,
                  width: 24,
                }}></View>
              <Text
                style={{
                  fontSize: 14,
                  color: '#201F26',
                  fontWeight: '400',
                  marginLeft: 23,
                }}>
                Repaint Sepatu
              </Text>
            </View>
            <View style={styles.checkBox}>
              <View
                style={{
                  borderWidth: 1,
                  borderRadius: 3,
                  borderColor: '#B8B8B8',
                  height: 24,
                  width: 24,
                }}></View>
              <Text
                style={{
                  fontSize: 14,
                  color: '#201F26',
                  fontWeight: '400',
                  marginLeft: 23,
                }}>
                Cuci Sepatu
              </Text>
            </View>
            <View style={styles.checkBox}>
              <View
                style={{
                  borderWidth: 1,
                  borderRadius: 3,
                  borderColor: '#B8B8B8',
                  height: 24,
                  width: 24,
                }}></View>
              <Text
                style={{
                  fontSize: 14,
                  color: '#201F26',
                  fontWeight: '400',
                  marginLeft: 23,
                }}>
                Ganti Sol Sepatu
              </Text>
            </View>
            <View style={{marginBottom: 25, marginTop: 44}}>
              <Text style={{color: 'red', fontWeight: 'bold'}}>Catatan</Text>
              <TextInput //component yang digunakan untuk memasukkan data yang kita inginkan
                placeholder="Cth : ingin ganti sol baru" //pada tampilan ini, kita ingin user memasukkan email
                style={{
                  marginTop: 10,
                  width: '100%',
                  height: 92,
                  textAlignVertical: 'top',
                  borderRadius: 8,
                  backgroundColor: '#F6F8FF',
                  paddingHorizontal: 10,
                }}
                keyboardType="default" //akan muncul tombol @ pada keyboard yang nanti akan memudahkan user mengisi email
              />
            </View>
            <View style={{marginBottom: 25}}>
              <Text style={{color: 'red', fontWeight: 'bold'}}>Kupon Promo</Text>
              <View style={{marginTop: 10,
                  width: '100%',
                  height: 47,
                  borderRadius: 8,
                  borderColor: '#C03A2B',
                  borderWidth: 1,
                  paddingHorizontal: 10,
                  flexDirection:'row',
                  justifyContent: 'flex-start',
                  alignItems:'center'
                  }}>
                <Text>Pilih kupon Promo</Text>
                <Image 
                source={require('../assets/icon/arrow_ic.png')} //load asset dari local
                style={{
                    marginLeft: 220,
                  width: 7,
                  height:12,
                }}
                />
              </View>
            </View>
          </View>
        </View>
        <View style={styles.cartButton}>
          <TouchableOpacity onPress={() => navigation.navigate('Keranjangs')}>
            <Text
              style={{
                fontSize: 16.5,
                color: '#FFFFFF',
                fontWeight: '700',
              }}>
              Masukan Keranjang
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};
export default FormPemesanan;
