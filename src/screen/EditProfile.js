import React from 'react';
import {
  View,
  Text,
  TextInput,
  ScrollView,
  Image,
  TouchableOpacity,
} from 'react-native';
import {styles} from './Style';
const EditProfile = ({navigation, route}) => {
  return (
    <View style={{flex: 1, backgroundColor: '#FFFFFF'}}>
       <View
        style={{
          width: '100%',
          height: 60,
          backgroundColor: '#FFFFFF',
          flexDirection: 'row',
          alignSelf: 'center',
          alignItems: 'center',
          paddingLeft: 14,
          shadowColor: '#000',
          shadowOffset: {
            width: 0,
            height: 1,
          },
          shadowOpacity: 0.22,
          shadowRadius: 2.22,

          elevation: 3,
        }}>
        <TouchableOpacity>
          <Image
          style={{
            size: 24,
            color: '#BB2427',
          }}
          source={require('../assets/icon/blackBack_ic.png')}
          />
        </TouchableOpacity>
        <Text
          style={{
            fontFamily: 'Montserrat',
            marginLeft: 14,
            fontWeight: '700',
            fontSize: 18,
            lineHeight: 17,
          }}>
          Edit Profile
        </Text>
      </View>
      <ScrollView //component yang digunakan agar tampilan kita bisa discroll
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{paddingBottom: 10}}>
        <View style={styles.editProfile}>
          <Image
            source={require('../assets/image/profile_photo.png')} //load asset dari local
            style={{
              width: 95.11,
              height: 95.11,
              resizeMode: 'contain',
            }}
          />
          <View style={styles.editButton}>
            <TouchableOpacity>
              <View style={{flexDirection: 'row'}}>
                <Image
                  source={require('../assets/icon/Edit_ic.png')} //load asset dari local
                  style={{
                    width: 20,
                    height: 20,
                  }}
                />
                <Text
                  style={{
                    fontSize: 18,
                    color: '#3A4BE0',
                    fontWeight: '500',
                    marginLeft: 15,
                    marginTop: -1,
                  }}>
                  Edit Foto
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.editBio}>
          <View style={{marginBottom: 25}}>
            <Text style={{color: 'red', fontWeight: 'bold'}}>Nama</Text>
            <TextInput //component yang digunakan untuk memasukkan data yang kita inginkan
              placeholder="Fahrii Fahriza" //pada tampilan ini, kita ingin user memasukkan email
              style={{
                marginTop: 10,
                width: '100%',
                borderRadius: 8,
                backgroundColor: '#F6F8FF',
                paddingHorizontal: 10,
              }}
              keyboardType="default" //akan muncul tombol @ pada keyboard yang nanti akan memudahkan user mengisi email
            />
          </View>
          <View style={{marginBottom: 25}}>
            <Text style={{color: 'red', fontWeight: 'bold'}}>Email</Text>
            <TextInput //component yang digunakan untuk memasukkan data yang kita inginkan
              placeholder="fahriifahriza@gmail.com" //pada tampilan ini, kita ingin user memasukkan email
              style={{
                marginTop: 10,
                width: '100%',
                borderRadius: 8,
                backgroundColor: '#F6F8FF',
                paddingHorizontal: 10,
              }}
              keyboardType="email-address" //akan muncul tombol @ pada keyboard yang nanti akan memudahkan user mengisi email
            />
          </View>
          <View style={{marginBottom: 25}}>
            <Text style={{color: 'red', fontWeight: 'bold'}}>No hp</Text>
            <TextInput //component yang digunakan untuk memasukkan data yang kita inginkan
              placeholder="08124564879" //pada tampilan ini, kita ingin user memasukkan email
              style={{
                marginTop: 10,
                width: '100%',
                borderRadius: 8,
                backgroundColor: '#F6F8FF',
                paddingHorizontal: 10,
              }}
              keyboardType="default" //akan muncul tombol @ pada keyboard yang nanti akan memudahkan user mengisi email
            />
          </View>
        </View>
        <View style={styles.simpanButton}>
          <TouchableOpacity onPress={() => navigation.navigate('Profile')}>
            <Text
              style={{
                fontSize: 16.5,
                color: '#FFFFFF',
                fontWeight: '700',
              }}>
              Simpan
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};
export default EditProfile;
