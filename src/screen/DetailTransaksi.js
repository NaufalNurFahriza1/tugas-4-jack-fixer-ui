import React from 'react';
import {
  View,
  Text,
  TextInput,
  ScrollView,
  Image,
  TouchableOpacity,
} from 'react-native';
import {styles} from './Style';

const DetailTransaksi = ({navigation, route}) => {
  return (
    <View style={styles.container}>
      <View
        style={{
          width: '100%',
          height: 60,
          backgroundColor: '#FFFFFF',
          flexDirection: 'row',
          alignSelf: 'center',
          alignItems: 'center',

          paddingLeft: 14,
          shadowColor: '#000',
          shadowOffset: {
            width: 0,
            height: 1,
          },
          shadowOpacity: 0.22,
          shadowRadius: 2.22,

          elevation: 3,
        }}>
        <TouchableOpacity onPress={() => navigation.navigate('Transaksi')}>
          <Image
            style={{
              size: 24,
              color: '#BB2427',
            }}
            source={require('../assets/icon/blackBack_ic.png')}
          />
        </TouchableOpacity>
      </View>
      <ScrollView //component yang digunakan agar tampilan kita bisa discroll
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{paddingBottom: 10}}>
        <View
          style={{
            width: '100%',
            height: 238,
            backgroundColor: '#FFFFFF',
            alignSelf: 'center',
            flexDirection: 'column',
            backgroundColor: '#FFFFFF',
            marginTop: 16,
            marginBottom: 10,
            alignItems: 'center',
          }}>
          <Text
            style={{
              paddingTop: 10,
              fontFamily: 'Monsserrat',
              fontWeight: '500',
              fontSize: 14,
              lineHeight: 17,
              color: '#979797',
            }}>
            20 Desember 2020 - 09.00
          </Text>
          <Text
            style={{
              fontFamily: 'Monsserrat',
              fontWeight: '700',
              fontSize: 36,
              lineHeight: 17,
              color: '#201F26',
              marginLeft: 14,
              paddingTop: 57,
            }}>
            CS2012321
          </Text>
          <Text style={{fontWeight: 'bold'}}>Kode Reservasi</Text>

          <Text
            style={{
              fontFamily: 'Monsserrat',
              fontWeight: '400',
              fontSize: 14,
              lineHeight: 17,
              color: '#6F6F6F',
              textAlign: 'center',
              paddingTop: 42,
              width: 238,
              paddingBottom: 24,
            }}>
            Sebutkan Kode Reservasi saat tiba di outlet
          </Text>
        </View>
        <Text
          style={{
            paddingTop: 10,
            marginLeft: 14,
            fontFamily: 'Monsserrat',
            fontWeight: '500',
            fontSize: 14,
            lineHeight: 17,
            color: '#979797',
          }}>
          Barang
        </Text>
        <View
          style={{
            backgroundColor: '#FFFFFF',
            flexDirection: 'column',
            marginVertical: 10,
            marginHorizontal: 11,
          }}>
          <View
            style={{
              width: 353,
              height: 128,
              flexDirection: 'row',
              backgroundColor: '#FFFFFF',
              alignSelf: 'flex-start',
              marginHorizontal: 10,
              borderRadius: 8,
              marginTop: 8,
            }}>
            <Image
              style={{alignSelf: 'center', marginLeft: 14}}
              source={require('../assets/image/sample_sepatu.png')}></Image>
            <View
              style={{
                flexDirection: 'column',
                alignSelf: 'center',
                paddingLeft: 13,
              }}>
              <Text
                style={{
                  fontFamily: 'Monsserrat',
                  fontWeight: '500',
                  fontSize: 12,
                  lineHeight: 15,
                  color: '#000000',
                }}>
                New Balance - Pink Abu - 40
              </Text>
              <Text
                style={{
                  fontFamily: 'Monsserrat',
                  fontWeight: '400',
                  fontSize: 12,
                  lineHeight: 15,
                  color: '#737373',
                  paddingTop: 11,
                }}>
                Cuci sepatu
              </Text>
              <Text style={{paddingTop: 10}}>Note : -</Text>
            </View>
          </View>
        </View>
        <Text
          style={{
            paddingTop: 10,
            marginLeft: 14,
            fontFamily: 'Monsserrat',
            fontWeight: '500',
            fontSize: 14,
            lineHeight: 17,
            color: '#979797',
          }}>
          Status Pesanan
        </Text>

        <View
          style={{
            backgroundColor: '#FFFFFF',
            flexDirection: 'column',
            marginVertical: 10,
            marginHorizontal: 11,
          }}>
          <View
            style={{
              width: 353,
              flexDirection: 'row',
              backgroundColor: '#FFFFFF',
              alignSelf: 'flex-start',
              marginHorizontal: 15,
              borderRadius: 8,
              marginTop: 8,
            }}>
            <View
              style={{flexDirection: 'column', marginTop: 14, marginBottom: 2}}>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                }}>
                <Image
                  style={{
                    height: 10,
                    width: 10,
                    marginRight: 10,
                    alignSelf: 'center',
                  }}
                  source={require('../assets/icon/Ellipse_ic.png')}
                />
                <Text
                  style={{
                    marginRight: 200,
                    fontFamily: 'Monsserrat',
                    fontWeight: '400',
                    fontSize: 14,
                    lineHeight: 17,
                    color: '#201F26',
                    textAlign:'center'
                  }}>
                  Telah Reservasi
                </Text>
                <Text style={{
                    fontFamily: 'Monsserrat',
                    fontWeight: '400',
                    fontSize: 12,
                    lineHeight: 17,
                    color: '#A5A5A5',
                    textAlign:'center'
                }}>09.00</Text>
              </View>
              <Text style={{
                marginLeft:22,
                marginBottom:17,
                fontFamily: 'Monsserrat',
                fontWeight: '400',
                fontSize: 12,
                lineHeight: 17,
                color: '#A5A5A5',
              }}>20 Desember 2020</Text>
            </View>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};
export default DetailTransaksi;
