import React from 'react';
import {
  View,
  Text,
  TextInput,
  ScrollView,
  Image,
  TouchableOpacity,
} from 'react-native';
import {styles} from './Style';
const Keranjangs = ({navigation, route}) => {
  return (
    <View style={styles.container}>
      <View
        style={{
          width: '100%',
          height: 60,
          backgroundColor: '#FFFFFF',
          flexDirection: 'row',
          alignSelf: 'center',
          alignItems: 'center',
          paddingLeft: 14,
          shadowColor: '#000',
          shadowOffset: {
            width: 0,
            height: 1,
          },
          shadowOpacity: 0.22,
          shadowRadius: 2.22,

          elevation: 3,
        }}>
        <TouchableOpacity onPress={() => navigation.navigate('FormPemesanan')}>
          <Image
          style={{
            size: 24,
            color: '#BB2427',
          }}
          source={require('../assets/icon/blackBack_ic.png')}
          />
        </TouchableOpacity>
        <Text
          style={{
            fontFamily: 'Montserrat',
            marginLeft: 14,
            fontWeight: '700',
            fontSize: 18,
            lineHeight: 17,
          }}>
          Keranjang
        </Text>
      </View>
      <ScrollView //component yang digunakan agar tampilan kita bisa discroll
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{paddingBottom: 10}}>
        <View style={styles.cartBox}>
        <Image
            style={{alignSelf: 'center', marginLeft: 14,}}
            source={require('../assets/image/sample_sepatu.png')}>
            </Image>
            <View style={{flexDirection: "column", alignSelf:"center", paddingLeft: 13, }}>
                <Text style={{fontFamily: 'Monsserrat', fontWeight: '500', fontSize: 12, lineHeight: 15,color: "#000000" }}>New Balance - Pink Abu - 40</Text>
                <Text style={{fontFamily: 'Monsserrat', fontWeight: '400', fontSize: 12, lineHeight: 15,color: "#737373", paddingTop: 11}}>Cuci sepatu</Text>
                <Text style={{paddingTop: 10}}>Note : -</Text>
            </View>
        </View>

        <View style={{flexDirection: "row", alignSelf: "center", marginBottom:280,}}>
        <TouchableOpacity style={{flexDirection: "row", alignSelf: "center"}}>
            <Image 
            source={require('../assets/icon/SquarePlus_ic.png')} //load asset dari local
            style={{
              width: 24,
              height: 24,
              marginRight:10,
            }}/>
           <Text style={{fontFamily: 'Monsserrat', fontWeight: '700', fontSize: 14, lineHeight: 17,color: "#BB2427",alignSelf:"center"}} >Tambah barang</Text>
        </TouchableOpacity>
        </View>

        <View style={styles.cartButton}>
          <TouchableOpacity onPress={() => navigation.navigate('Summary')}>
            <Text
              style={{
                fontSize: 16.5,
                color: '#FFFFFF',
                fontWeight: '700',
              }}>
              Selanjutnya
            </Text>
          </TouchableOpacity>
        </View>

      </ScrollView>
    </View>
  );
};
export default Keranjangs;
