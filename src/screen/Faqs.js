import React, { useState } from 'react';
import {
    View,
    Text,
    TextInput,
    ScrollView,
    Image,
    TouchableOpacity,
} from 'react-native';
import { styles } from './Style';
const Faqs = ({ navigation, route }) => {
    const [show, setShow] = useState(false);
    return (
        <View style={styles.container}>
             <View
        style={{
          width: '100%',
          height: 60,
          backgroundColor: '#FFFFFF',
          flexDirection: 'row',
          alignSelf: 'center',
          alignItems: 'center',
          paddingLeft: 14,
          shadowColor: '#000',
          shadowOffset: {
            width: 0,
            height: 1,
          },
          shadowOpacity: 0.22,
          shadowRadius: 2.22,

          elevation: 3,
        }}>
        <TouchableOpacity onPress={() => navigation.navigate('Profile')}>
          <Image
          style={{
            size: 24,
            color: '#BB2427',
          }}
          source={require('../assets/icon/blackBack_ic.png')}
          />
        </TouchableOpacity>
        <Text
          style={{
            fontFamily: 'Montserrat',
            marginLeft: 14,
            fontWeight: '700',
            fontSize: 18,
            lineHeight: 17,
          }}>
          FAQ
        </Text>
      </View>
            <ScrollView //component yang digunakan agar tampilan kita bisa discroll
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{ paddingBottom: 10 }}>
                <View style={{paddingTop:33, paddingHorizontal:6, paddingBottom:8,}}>
                <TouchableOpacity style={styles.faqBox} onPress={() => setShow(show === 1 ? false : 1)}>
                    <View style={{ flexDirection: 'row', }}>
                        <Text style={{color:'#000000',}}>Lorem ipsum dolor sit amet, consectetur adipiscing elit?</Text>
                        <Image
                            style={{ alignSelf: 'center', marginLeft:19, }}
                            source={show === 1 ? require('../assets/icon/chevron_up_ic.png') :  require('../assets/icon/chevron_down_ic.png')}
                        />
                    </View>
                    {
                        show === 1 ? 
                        <Text style={{color:'#595959', paddingTop:11, paddingBottom:14,}}>Lorem ipsum dolor sit amet, consectetur adipiscing elit?</Text>
                        : null
                    }
                </TouchableOpacity>
                </View>
                <View style={{paddingTop:16, paddingHorizontal:6, paddingBottom:8,}}>
                <TouchableOpacity style={styles.faqBox} onPress={() => setShow(show === 1 ? false : 1)}>
                    <View style={{ flexDirection: 'row', }}>
                        <Text style={{color:'#000000',}}>Lorem ipsum dolor sit amet, consectetur adipiscing elit?</Text>
                        <Image
                            style={{ alignSelf: 'center', marginLeft:19, }}
                            source={show === 1 ? require('../assets/icon/chevron_up_ic.png') :  require('../assets/icon/chevron_down_ic.png')}
                        />
                    </View>
                    {
                        show === 1 ? 
                        <Text style={{color:'#595959', paddingTop:11, paddingBottom:14,}}>Lorem ipsum dolor sit amet, consectetur adipiscing elit?</Text>
                        : null
                    }
                </TouchableOpacity>
                </View>
                <View style={{paddingTop:16, paddingHorizontal:6, paddingBottom:8,}}>
                <TouchableOpacity style={styles.faqBox} onPress={() => setShow(show === 1 ? false : 1)}>
                    <View style={{ flexDirection: 'row', }}>
                        <Text style={{color:'#000000',}}>Lorem ipsum dolor sit amet, consectetur adipiscing elit?</Text>
                        <Image
                            style={{ alignSelf: 'center', marginLeft:19, }}
                            source={show === 1 ? require('../assets/icon/chevron_up_ic.png') :  require('../assets/icon/chevron_down_ic.png')}
                        />
                    </View>
                    {
                        show === 1 ? 
                        <Text style={{color:'#595959', paddingTop:11, paddingBottom:14,}}>Lorem ipsum dolor sit amet, consectetur adipiscing elit?</Text>
                        : null
                    }
                </TouchableOpacity>
                </View>
                <View style={{paddingTop:16, paddingHorizontal:6, paddingBottom:8,}}>
                <TouchableOpacity style={styles.faqBox} onPress={() => setShow(show === 1 ? false : 1)}>
                    <View style={{ flexDirection: 'row', }}>
                        <Text style={{color:'#000000',}}>Lorem ipsum dolor sit amet, consectetur adipiscing elit?</Text>
                        <Image
                            style={{ alignSelf: 'center', marginLeft:19, }}
                            source={show === 1 ? require('../assets/icon/chevron_up_ic.png') :  require('../assets/icon/chevron_down_ic.png')}
                        />
                    </View>
                    {
                        show === 1 ? 
                        <Text style={{color:'#595959', paddingTop:11, paddingBottom:14,}}>Lorem ipsum dolor sit amet, consectetur adipiscing elit?</Text>
                        : null
                    }
                </TouchableOpacity>
                </View>
                <View style={{paddingTop:16, paddingHorizontal:6, paddingBottom:8,}}>
                <TouchableOpacity style={styles.faqBox} onPress={() => setShow(show === 1 ? false : 1)}>
                    <View style={{ flexDirection: 'row', }}>
                        <Text style={{color:'#000000',}}>Lorem ipsum dolor sit amet, consectetur adipiscing elit?</Text>
                        <Image
                            style={{ alignSelf: 'center', marginLeft:19, }}
                            source={show === 1 ? require('../assets/icon/chevron_up_ic.png') :  require('../assets/icon/chevron_down_ic.png')}
                        />
                    </View>
                    {
                        show === 1 ? 
                        <Text style={{color:'#595959', paddingTop:11, paddingBottom:14,}}>Lorem ipsum dolor sit amet, consectetur adipiscing elit?</Text>
                        : null
                    }
                </TouchableOpacity>
                </View>
                <View style={{paddingTop:16, paddingHorizontal:6, paddingBottom:8,}}>
                <TouchableOpacity style={styles.faqBox} onPress={() => setShow(show === 1 ? false : 1)}>
                    <View style={{ flexDirection: 'row', }}>
                        <Text style={{color:'#000000',}}>Lorem ipsum dolor sit amet, consectetur adipiscing elit?</Text>
                        <Image
                            style={{ alignSelf: 'center', marginLeft:19, }}
                            source={show === 1 ? require('../assets/icon/chevron_up_ic.png') :  require('../assets/icon/chevron_down_ic.png')}
                        />
                    </View>
                    {
                        show === 1 ? 
                        <Text style={{color:'#595959', paddingTop:11, paddingBottom:14,}}>Lorem ipsum dolor sit amet, consectetur adipiscing elit?</Text>
                        : null
                    }
                </TouchableOpacity>
                </View>
            </ScrollView>
        </View>
    );
};
export default Faqs;
