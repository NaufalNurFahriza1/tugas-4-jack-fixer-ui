import React from 'react';
import {
  View,
  Text,
  ScrollView,
  KeyboardAvoidingView,
  Image,
  TouchableOpacity,
} from 'react-native';
import {styles} from './Style';
const Home = ({navigation, route}) => {
  return (
    <View style={styles.container}>
      <ScrollView //component yang digunakan agar tampilan kita bisa discroll
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{paddingBottom: 10}}>
          <View style={styles.header}>
            <View style={styles.photoBag}>
              <TouchableOpacity>
                <Image
                  source={require('../assets/image/profile_photo.png')} //load asset dari local
                  style={{
                    width: 45,
                    height: 45,
                    resizeMode: 'contain',
                  }}
                />
              </TouchableOpacity>
              <TouchableOpacity>
                <Image
                  source={require('../assets/icon/Bag_ic.png')} //load asset dari local
                  style={{
                    width: 24,
                    height: 24,
                    resizeMode: 'contain',
                  }}
                />
              </TouchableOpacity>
            </View>

            <View style={styles.textHeader}>
              <Text
                style={{
                  fontSize: 15,
                  color: '#034262',
                  fontFamily: 'Montserrat',
                  fontStyle: 'normal',
                  fontWeight: '500',
                  marginVertical: 5,
                }}>
                Hello Fahri!
              </Text>
              <Text
                style={{
                  fontSize: 20,
                  color: '#0A0827',
                  fontFamily: 'Montserrat',
                  fontStyle: 'normal',
                  fontWeight: '700',
                }}>
                Ingin merawat dan perbaiki sepatumu? cari disini
              </Text>
            </View>

            <View style={styles.searchFilter}>
              <View
                style={{
                  justifyContent: 'center',
                  paddingLeft: 10,
                  width: 275,
                  height: 45,
                  backgroundColor: '#F6F8FF',
                  borderRadius: 13,
                }}>
                <Image
                  source={require('../assets/icon/Search_ic.png')} //load asset dari local
                  style={{
                    width: 24,
                    height: 24,
                    resizeMode: 'contain',
                  }}
                />
              </View>
              <View
                style={{
                  alignItems: 'center',
                  justifyContent: 'center',
                  width: 45,
                  height: 45,
                  backgroundColor: '#F6F8FF',
                  borderRadius: 13,
                }}>
                <Image
                  source={require('../assets/icon/Filter_ic.png')} //load asset dari local
                  style={{
                    width: 24,
                    height: 24,
                    resizeMode: 'contain',
                  }}
                />
              </View>
            </View>
          </View>
          <View style={styles.categoryBox}>
            <View style={styles.categoryItems}>
              <TouchableOpacity>
                <Image
                  source={require('../assets/icon/Sepatu_ic.png')} //load asset dari local
                  style={{
                    width: 45,
                    height: 45,
                    resizeMode: 'contain',
                  }}
                />
                <Text
                  style={{
                    fontSize: 9,
                    color: '#BB2427',
                    fontWeight: '600',
                    textAlign: 'center',
                    marginTop: 10,
                  }}>
                  Sepatu
                </Text>
              </TouchableOpacity>
            </View>
            <View style={styles.categoryItems}>
              <TouchableOpacity>
                <Image
                  source={require('../assets/icon/Jaket_ic.png')} //load asset dari local
                  style={{
                    width: 45,
                    height: 45,
                    resizeMode: 'contain',
                  }}
                />
                <Text
                  style={{
                    fontSize: 9,
                    color: '#BB2427',
                    fontWeight: '600',
                    textAlign: 'center',
                    marginTop: 10,
                  }}>
                  Jaket
                </Text>
              </TouchableOpacity>
            </View>
            <View style={styles.categoryItems}>
              <TouchableOpacity>
                <Image
                  source={require('../assets/icon/Tas_ic.png')} //load asset dari local
                  style={{
                    width: 45,
                    height: 45,
                    resizeMode: 'contain',
                  }}
                />
                <Text
                  style={{
                    fontSize: 9,
                    color: '#BB2427',
                    fontWeight: '600',
                    textAlign: 'center',
                    marginTop: 10,
                  }}>
                  Tas
                </Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.recomendBox}>
            <View
              style={{
                justifyContent: 'space-between',
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <View style={styles.recomendText}>
              <Text
                style={{
                  fontSize: 16,
                  color: '#0A0827',
                  fontWeight: '600',
                  marginRight: 75,
                }}>
                Rekomendasi Terdekat
              </Text>
              <Text
                style={{
                  fontSize: 14,
                  color: '#E64C3C',
                  fontWeight: '500',
                  marginLeft: 75,
                }}>
                View All
              </Text>
              </View>
            </View>

            <View style={styles.recomendItems}>
              <View>
                <Image
                  source={require('../assets/image/repair_Gejayan.png')} //load asset dari local
                  style={{
                    borderRadius: 5,
                    width: 80,
                    height: 121,
                    resizeMode: 'contain',
                  }}
                />
              </View>
              <View style={{marginHorizontal: 20}}>
                <View style={styles.starheart}>
                  <View style={styles.stars}>
                    <Image
                      source={require('../assets/icon/fullstar_ic.png')} //load asset dari local
                      style={{
                        width: 12.25,
                        height: 11.64,
                        resizeMode: 'contain',
                      }}
                    />
                    <Image
                      source={require('../assets/icon/fullstar_ic.png')} //load asset dari local
                      style={{
                        width: 12.25,
                        height: 11.64,
                        resizeMode: 'contain',
                      }}
                    />
                    <Image
                      source={require('../assets/icon/fullstar_ic.png')} //load asset dari local
                      style={{
                        width: 12.25,
                        height: 11.64,
                        resizeMode: 'contain',
                      }}
                    />
                    <Image
                      source={require('../assets/icon/fullstar_ic.png')} //load asset dari local
                      style={{
                        width: 12.25,
                        height: 11.64,
                        resizeMode: 'contain',
                      }}
                    />
                    <Image
                      source={require('../assets/icon/linestar_ic.png')} //load asset dari local
                      style={{
                        width: 12.25,
                        height: 11.64,
                        resizeMode: 'contain',
                      }}
                    />
                  </View>
                  <Image
                    source={require('../assets/icon/Heart_ic.png')} //load asset dari local
                    style={{
                      width: 20.38,
                      height: 19.02,
                      resizeMode: 'contain',
                      marginLeft: 170,
                    }}
                  />
                </View>
                <Text
                  style={{
                    fontSize: 10,
                    color: '#D8D8D8',
                    fontWeight: '500',
                    marginLeft: 2,
                  }}>
                  4.8 Ratings
                </Text>
                <Text
                  style={{
                    fontSize: 12,
                    color: '#201F26',
                    fontWeight: '600',
                    marginLeft: 2,
                    paddingVertical: 7,
                  }}>
                  Jack Repair Gejayan
                </Text>
                <Text
                  style={{
                    fontSize: 9,
                    color: '#D8D8D8',
                    fontWeight: '500',
                    marginLeft: 2,
                    paddingBottom: 3,
                  }}>
                  Jl. Gejayan III No.2, Karangasem, Kec. Laweyan . . .
                </Text>
                <TouchableOpacity>
                  <View
                    style={{
                      width: 55,
                      height: 21,
                      borderRadius: 10.5,
                      backgroundColor: 'rgba(230, 76, 60, 0.2)',
                      alignItems: 'center',
                      justifyContent: 'center',
                      marginVertical: 5,
                    }}>
                    <Text style={{
                     fontSize: 12,
                     color: '#EA3D3D',
                     fontWeight: '700',
                    }}>TUTUP</Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>

            <View style={styles.recomendItems}>
              <View>
                <Image
                  source={require('../assets/image/repair_Seturan.png')} //load asset dari local
                  style={{
                    borderRadius: 5,
                    width: 80,
                    height: 121,
                    resizeMode: 'contain',
                  }}
                />
              </View>
              <View style={{marginHorizontal: 20}}>
                <View style={styles.starheart}>
                  <View style={styles.stars}>
                    <Image
                      source={require('../assets/icon/fullstar_ic.png')} //load asset dari local
                      style={{
                        width: 12.25,
                        height: 11.64,
                        resizeMode: 'contain',
                      }}
                    />
                    <Image
                      source={require('../assets/icon/fullstar_ic.png')} //load asset dari local
                      style={{
                        width: 12.25,
                        height: 11.64,
                        resizeMode: 'contain',
                      }}
                    />
                    <Image
                      source={require('../assets/icon/fullstar_ic.png')} //load asset dari local
                      style={{
                        width: 12.25,
                        height: 11.64,
                        resizeMode: 'contain',
                      }}
                    />
                    <Image
                      source={require('../assets/icon/fullstar_ic.png')} //load asset dari local
                      style={{
                        width: 12.25,
                        height: 11.64,
                        resizeMode: 'contain',
                      }}
                    />
                    <Image
                      source={require('../assets/icon/linestar_ic.png')} //load asset dari local
                      style={{
                        width: 12.25,
                        height: 11.64,
                        resizeMode: 'contain',
                      }}
                    />
                  </View>
                  <Image
                    source={require('../assets/icon/lineHeart_ic.png')} //load asset dari local
                    style={{
                      width: 20.38,
                      height: 19.02,
                      resizeMode: 'contain',
                      marginLeft: 170,
                    }}
                  />
                </View>
                <Text
                  style={{
                    fontSize: 10,
                    color: '#D8D8D8',
                    fontWeight: '500',
                    marginLeft: 2,
                  }}>
                  4.7 Ratings
                </Text>
                <Text
                  style={{
                    fontSize: 12,
                    color: '#201F26',
                    fontWeight: '600',
                    marginLeft: 2,
                    paddingVertical: 7,
                  }}>
                  Jake Repair Seturan
                </Text>
                <Text
                  style={{
                    fontSize: 9,
                    color: '#D8D8D8',
                    fontWeight: '500',
                    marginLeft: 2,
                    paddingBottom: 3,
                  }}>
                  Jl. Seturan Kec. Laweyan . . .
                </Text>
                <TouchableOpacity onPress={() => navigation.navigate('DetailScreen')}>
                  <View
                    style={{
                      width: 55,
                      height: 21,
                      borderRadius: 10.5,
                      backgroundColor: 'rgba(17, 168, 78, 0.12)',
                      alignItems: 'center',
                      justifyContent: 'center',
                      marginVertical: 5,
                    }}>
                    <Text style={{
                     fontSize: 12,
                     color: '#11A84E',
                     fontWeight: '700',
                    }}>BUKA</Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </View>
      </ScrollView>
    </View>
  );
};
export default Home;
