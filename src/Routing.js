import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { NavigationContainer } from "@react-navigation/native";

import SplashScreen from "./screen/SplashScreen";
import AuthNavigation from "./AuthNavigation";
import DetailScreen from "./screen/DetailScreen";
import FormPemesanan from "./screen/FormPemesanan";
import Keranjangs from "./screen/Keranjangs";
import Summary from "./screen/Summary";
import Profile from "./screen/Profile";
import EditProfile from "./screen/EditProfile";
import Faqs from "./screen/Faqs";
import Checkout from "./screen/Checkout";
import Reservasi from "./screen/Reservasi";
import Transaksi from "./screen/Transaksi";
import DetailTransaksi from "./screen/DetailTransaksi";

const Stack = createStackNavigator();

export default function Routing() {
    return (
        <NavigationContainer>
            <Stack.Navigator screenOptions={{headerShown: false}}>
                <Stack.Screen name="SplashScreen" component={SplashScreen}/>
                <Stack.Screen name="AuthNavigation" component={AuthNavigation}/>
                <Stack.Screen name="DetailScreen" component={DetailScreen}/>
                <Stack.Screen name="FormPemesanan" component={FormPemesanan}/>
                <Stack.Screen name="Keranjangs" component={Keranjangs}/>
                <Stack.Screen name="Summary" component={Summary}/>
                <Stack.Screen name="Checkout" component={Checkout}/>
                <Stack.Screen name="Reservasi" component={Reservasi}/>
                <Stack.Screen name="Transaksi" component={Transaksi}/>
                <Stack.Screen name="DetailTransaksi" component={DetailTransaksi}/>
                <Stack.Screen name="Profile" component={Profile}/>
                <Stack.Screen name="EditProfile" component={EditProfile}/>
                <Stack.Screen name="Faqs" component={Faqs}/>
            </Stack.Navigator>
        </NavigationContainer>
    )
}